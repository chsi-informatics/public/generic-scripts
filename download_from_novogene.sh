#!/usr/bin/env bash

#SBATCH --partition=chsi
#SBATCH --account=chsi
#SBATCH --job-name="novogene_download"
#SBATCH --output="%x-%A-%a.log"

# If you aren't part of the chsi group on DCC, you should edit or delete "--partition" and "--account" above

# After setting values in the following section, this script can be run with this command: "sbatch download_from_novogene.sh"

#----------------------------------------------
# In the following section, set the following by putting the correct values within the quotes
#----------------------------------------------
DOWNLOAD_DIR="" # Set this to the directory (absolute path) where you want downloaded data to go. The directory will be created automatically if it does not already exist
NOVOGENE_ACCOUNT="" #Set this with the 'Account' provided in the email from Novogene
NOVOGENE_PASS="" #Set this with the 'Password' provided in the email from Novogene

SERVER_URL="ftp://usftp21.novogene.com" # Change this is the email from Novogene lists a different 


#----------------------------------------------
# Do not modify anything below here
#----------------------------------------------
set -u

mkdir -p $DOWNLOAD_DIR

wget --tries=200 --waitretry=1 --continue --directory-prefix $DOWNLOAD_DIR --recursive --user="$NOVOGENE_ACCOUNT" --password="$NOVOGENE_PASS" $SERVER_URL

cd ${DOWNLOAD_DIR}/usftp21.novogene.com
md5sum -c MD5.txt
